package cc.emmert.tisadvanced.module.asic;

import java.util.List;

import javax.annotation.Nullable;

import cc.emmert.tisadvanced.module.asic.ASICModule.MODE;
import li.cil.tis3d.api.API;
import li.cil.tis3d.common.item.ModuleItem;
import li.cil.tis3d.util.EnumUtils;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ASICModuleItem extends ModuleItem {

    public ASICModuleItem() {
        super(createProperties().stacksTo(1).tab(API.itemGroup));
    }

    @Override
    public InteractionResultHolder<ItemStack> use(final Level level, final Player player, final InteractionHand hand) {
        ItemStack stack = player.getItemInHand(hand);
        MODE mode = loadFromStack(stack);

        MODE nextMode;
        if(player.isShiftKeyDown()) {
            nextMode = mode.prev();
        } else {
            nextMode = mode.next();
        }

        saveToStack(stack, nextMode);
        if(level.isClientSide()) {
            player.displayClientMessage(new TextComponent(nextMode.name()), true);
        }
        return InteractionResultHolder.sidedSuccess(player.getItemInHand(hand), level.isClientSide());
    }

    @Override
    public void appendHoverText(ItemStack stack, @Nullable Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        MODE mode = loadFromStack(stack);

        tooltip.add(new TextComponent("Mode: " + mode.toString()));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    public static MODE loadFromTag(@Nullable final CompoundTag tag) {
        if (tag != null) {
            return EnumUtils.load(MODE.class, ASICModule.TAG_MODE, tag);
        } else {
            return MODE.SIN;
        }
    }

    public static MODE loadFromStack(final ItemStack stack) {
        return loadFromTag(stack.getTag());
    }

    public static void saveToStack(final ItemStack stack, final MODE mode) {
        final CompoundTag tag = stack.getOrCreateTag();
        EnumUtils.save(mode, ASICModule.TAG_MODE, tag);
    }
}
