package cc.emmert.tisadvanced.module.asic;

import com.mojang.blaze3d.vertex.PoseStack;

import cc.emmert.tisadvanced.TISAdvanced;
import cc.emmert.tisadvanced.util.HalfFloat;
import cc.emmert.tisadvanced.util.NumUtils;
import li.cil.manual.api.render.FontRenderer;
import li.cil.tis3d.api.API;
import li.cil.tis3d.api.machine.Casing;
import li.cil.tis3d.api.machine.Face;
import li.cil.tis3d.api.machine.HaltAndCatchFireException;
import li.cil.tis3d.api.machine.Pipe;
import li.cil.tis3d.api.machine.Port;
import li.cil.tis3d.api.prefab.module.AbstractModuleWithRotation;
import li.cil.tis3d.api.util.RenderContext;
import li.cil.tis3d.util.Color;
import li.cil.tis3d.util.EnumUtils;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class ASICModule extends AbstractModuleWithRotation {
    static final String TAG_MODE = "mode";

    enum STATE {
        CALCULATE_ON_NEXT_INPUT,
        AWAITING_PARAMS,
    }

    enum MODE {
        SIN,
        COS,
        TAN,

        SQRT,
        CBRT,

        EXP,
        LN,
        HYPT,

        ASIN,
        ACOS,
        ATAN,

        SINH,
        COSH,
        TANH;

        MODE next() {
            if(this.ordinal() == MODE.values().length - 1) {
                return MODE.values()[0];
            } else {
                return MODE.values()[this.ordinal() + 1];
            }
        }

        MODE prev() {
            if(this.ordinal() == 0) {
                return MODE.values()[MODE.values().length - 1];
            } else {
                return MODE.values()[this.ordinal() - 1];
            }
        }

        int paramCount() {
            return switch(this) {
                case HYPT -> 2;
                default -> 1;
            };
        }
    }

    private MODE mode;
    
    // Only used for two-parameter functions
    private double firstParam;
    private STATE state;

    public ASICModule(Casing casing, Face face) {
        super(casing, face);
        this.mode = MODE.SIN;
        this.state = STATE.AWAITING_PARAMS;
    }

    @Override
    public void step() {
        this.stepInput();
    }

    private void stepInput() {
        for(final Port port : Port.VALUES) {
            final Pipe receivingPipe = getCasing().getReceivingPipe(getFace(), port);
            if (!receivingPipe.isReading()) {
                receivingPipe.beginRead();
            }
            if (receivingPipe.canTransfer()) {
                double input = HalfFloat.toFloat(receivingPipe.read());
                double output = 0;
                
                switch(this.mode) {
                    case SIN -> {
                        output = Math.sin(Math.toRadians(input));
                    }
                    case COS -> {
                        output = Math.cos(Math.toRadians(input));
                    }
                    case TAN -> {
                        output = Math.tan(Math.toRadians(input));
                    }
                    
                    case SQRT -> {
                        output = Math.sqrt(input);
                    }
                    case CBRT -> {
                        output = Math.cbrt(input);
                    }

                    case EXP -> {
                        output = Math.exp(input);
                    }
                    case LN -> {
                        output = Math.log(input);
                    }
                    case HYPT -> {
                        this.handleHypotenuse(input);
                    }
                    
                    case ASIN -> {
                        output = Math.toDegrees(Math.asin(input));
                    }
                    case ACOS -> {
                        output = Math.toDegrees(Math.acos(input));
                    }
                    case ATAN -> {
                        output = Math.toDegrees(Math.atan(input));
                    }
                    
                    case SINH -> {
                        output = Math.sinh(Math.toRadians(input));
                    }
                    case COSH -> {
                        output = Math.cosh(Math.toRadians(input));
                    }
                    case TANH -> {
                        output = Math.tanh(Math.toRadians(input));
                    }
                }

                if(Double.isNaN(output) || Double.isInfinite(output))
                    throw new HaltAndCatchFireException();
                if(this.mode.paramCount() < 2)
                    this.stepOutput((float) output);
            }
        }
    }

    private void handleHypotenuse(double input) {
        switch(state) {
            case AWAITING_PARAMS:
                this.firstParam = input;
                this.state = STATE.CALCULATE_ON_NEXT_INPUT;
                break;
            case CALCULATE_ON_NEXT_INPUT:
                this.stepOutput((float) Math.hypot(firstParam, input));
                this.state = STATE.AWAITING_PARAMS;
            }
    }

    private void stepOutput(float output) {
        this.cancelWrite();
        for (final Port port : Port.VALUES) {
            final Pipe sendingPipe = getCasing().getSendingPipe(getFace(), port);
            if (!sendingPipe.isWriting()) {
                output = NumUtils.clampFloat(output);
                sendingPipe.beginWrite(HalfFloat.toHalf(output));
            }
        }
    }

    @Override
    public void onInstalled(final ItemStack stack) {
        this.mode = ASICModuleItem.loadFromStack(stack);
    }

    @Override
    public void onUninstalled(final ItemStack stack) {
        ASICModuleItem.saveToStack(stack, this.mode);
    }

    @Override
    public void onBeforeWriteComplete(final Port port) {
        // If one completes, cancel all other writes to ensure a value is only
        // written once.
        this.cancelWrite();
    }

    @Override
    public void onWriteComplete(final Port port) {
        // Re-cancel in case step() was called after onBeforeWriteComplete() to
        // ensure we're not writing while waiting for input.
        this.cancelWrite();
    }

    @Override
    public void load(final CompoundTag tag) {
        super.load(tag);
        this.mode = EnumUtils.load(MODE.class, TAG_MODE, tag);
    }

    @Override
    public void save(final CompoundTag tag) {
        super.save(tag);
        EnumUtils.save(this.mode, TAG_MODE, tag);
    }

    @OnlyIn(Dist.CLIENT)
    public void render(final RenderContext context) {
        final PoseStack matrixStack = context.getMatrixStack();
        matrixStack.pushPose();
        this.rotateForRendering(matrixStack);
        context.drawAtlasQuadUnlit(new ResourceLocation(TISAdvanced.MOD_ID,"block/overlay/asic_module"));

        if (context.closeEnoughForDetails(getCasing().getPosition())) {
            drawState(context);
        }

        matrixStack.popPose();
    }

    @OnlyIn(Dist.CLIENT)
    private void drawState(RenderContext context) {
        final PoseStack matrixStack = context.getMatrixStack();
        final FontRenderer font = API.normalFontRenderer;

        matrixStack.translate(3 / 16f, 5 / 16f, 0);
        matrixStack.scale(1 / 64f, 1 / 64f, 1);

        // Brute-force centering of the function name string
        switch(this.mode.name().length()) {
            case 4 -> matrixStack.translate(2.5f, 5f, 0);
            case 3 -> matrixStack.translate(7.25f, 5f, 0);
            case 2 -> matrixStack.translate(12.5f, 5f, 0);
        }

        context.drawString(font, this.mode.name(), Color.WHITE);
    }
}
