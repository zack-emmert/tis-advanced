package cc.emmert.tisadvanced.module.radio;

import li.cil.tis3d.api.API;
import li.cil.tis3d.common.item.ModuleItem;

public class RadioModuleItem extends ModuleItem {
    public RadioModuleItem() {
        super(createProperties().stacksTo(1).tab(API.itemGroup));
    }
}
