package cc.emmert.tisadvanced.module.radio;

public class RadioPacket {
    private RadioReceiver source;
    private short packet;

    public RadioPacket(RadioReceiver source, short packet) {
        this.source = source;
        this.packet = packet;
    }

    public RadioReceiver getSource() {
        return this.source;
    }

    public short getData() {
        return this.packet;
    }
}
