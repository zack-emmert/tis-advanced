package cc.emmert.tisadvanced.module.radio;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import cc.emmert.tisadvanced.TISAdvanced;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.saveddata.SavedData;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;

public class RadioNetwork extends SavedData {
    public static final DeferredRegister<RadioProvider> RADIOS = DeferredRegister.create(RadioProvider.REGISTRY, TISAdvanced.MOD_ID);
    private static final Supplier<IForgeRegistry<RadioProvider>> RADIO_PROVIDERS_REGISTRY = RADIOS.makeRegistry(RadioProvider.class, RegistryBuilder::new);

    private Set<RadioReceiver> radios;

    private RadioNetwork(Set<RadioReceiver> radios) {
        this.radios = radios;
    }

    private RadioNetwork() {
        this.radios = new HashSet<>();
    }

    public static RadioNetwork get(Level level) {
        if(level instanceof ServerLevel serverLevel) {
            return serverLevel.getDataStorage().computeIfAbsent(load(level), RadioNetwork::new, "radios");
        } else {
            throw new RuntimeException("Attempted to load RadioNetwork from client");
        }
    }

    public void registerRadio(RadioReceiver radio) {
        if(!this.radios.contains(radio)) {
            radios.add(radio);
            this.setDirty();
        }
    }

    public void deregisterRadio(RadioReceiver radio) {
        if(this.radios.contains(radio)) {
            radios.remove(radio);
            this.setDirty();
        }
    }

    public boolean isRegistered(RadioReceiver radio) {
        return this.radios.contains(radio);
    }

    public void broadcastPacket(RadioPacket packet) {
        radios.stream()
            .filter((radio) -> radio != packet.getSource())
            .filter((radio) -> {
                Vec3 sourcePos = packet.getSource().getPos();
                return sourcePos.closerThan(radio.getPos(),256);
            })
            .forEach((radio) -> radio.onRadioPacket(packet));

    }

    @Override
    public CompoundTag save(CompoundTag tag) {
        ListTag radiosTag = new ListTag();

        for(RadioReceiver radio: radios) {
            CompoundTag radioTag = new CompoundTag();
            RadioProvider provider = radio.getProvider();
            radioTag.putString("__provider", RADIO_PROVIDERS_REGISTRY.get().getKey(provider).toString());
            provider.save(radio,radioTag);
            radiosTag.add(radioTag);
        }

        tag.put("radios",radiosTag);

        return tag;
    }

    private static Function<CompoundTag,RadioNetwork> load(Level level) {
        return (tag) -> {
            Set<RadioReceiver> radios = new HashSet<>();
            ListTag radiosTag = tag.getList("radios",Tag.TAG_COMPOUND);

            for(Tag t : radiosTag) {
                CompoundTag radioTag = (CompoundTag) t;
                RadioProvider provider = RADIO_PROVIDERS_REGISTRY.get().getValue(new ResourceLocation(radioTag.getString("__provider")));
                provider.load(level, radioTag).ifPresent(radios::add);
            }

            return new RadioNetwork(radios);
        };
    }
}
