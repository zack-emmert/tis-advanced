package cc.emmert.tisadvanced.module.radio;

import java.util.Optional;

import net.minecraft.core.Registry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.IForgeRegistryEntry;

public interface RadioProvider extends IForgeRegistryEntry<RadioProvider> {
    public static final ResourceKey<Registry<RadioProvider>> REGISTRY = ResourceKey.createRegistryKey(new ResourceLocation("tisadvanced", "radio"));
    

    public Optional<RadioReceiver> load(Level level, CompoundTag tag);
    public void save(RadioReceiver radio, CompoundTag tag);
}
