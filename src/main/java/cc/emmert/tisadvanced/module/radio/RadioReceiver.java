package cc.emmert.tisadvanced.module.radio;

import net.minecraft.world.phys.Vec3;

public interface RadioReceiver {

    public void onRadioPacket(RadioPacket packet);
    public Vec3 getPos();
    public RadioProvider getProvider();
}
