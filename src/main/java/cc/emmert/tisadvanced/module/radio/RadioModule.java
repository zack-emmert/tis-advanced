package cc.emmert.tisadvanced.module.radio;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Optional;

import com.mojang.blaze3d.vertex.PoseStack;

import cc.emmert.tisadvanced.TISAdvanced;
import li.cil.tis3d.api.machine.Casing;
import li.cil.tis3d.api.machine.Face;
import li.cil.tis3d.api.machine.Pipe;
import li.cil.tis3d.api.machine.Port;
import li.cil.tis3d.api.prefab.module.AbstractModuleWithRotation;
import li.cil.tis3d.api.util.RenderContext;
import li.cil.tis3d.common.CommonConfig;
import li.cil.tis3d.util.Color;
import li.cil.tis3d.util.EnumUtils;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.registries.ForgeRegistryEntry;
public class RadioModule extends AbstractModuleWithRotation implements RadioReceiver {

    private Deque<Short> packetQueue;
    private int transmissionIndicatorCooldown;

    // These two values are synced to the client to manage the indicator lights
    private boolean transmissionIndicatorActive;
    private boolean queueIndicatorGreen;

    public RadioModule(Casing casing, Face face) {
        super(casing, face);
        this.packetQueue = new ArrayDeque<>();
    }

    @Override
    public void step() {
        if(this.transmissionIndicatorCooldown > 0)
            this.transmissionIndicatorCooldown--;
        else {
            this.setTransmissionIndicator(false);
        }
        
        this.stepInput();
        this.stepOutput();
    }

    private void stepInput() {
        for (final Port port : Port.VALUES) {
            // Continuously read from all ports, emit packet when receiving a value.
            final Pipe receivingPipe = this.getCasing().getReceivingPipe(this.getFace(), port);
            if (!receivingPipe.isReading()) {
                receivingPipe.beginRead();
            }
            if (receivingPipe.canTransfer() && !this.getCasing().getCasingLevel().isClientSide()) {
                RadioNetwork.get(this.getCasing().getCasingLevel()).broadcastPacket(new RadioPacket(this,receivingPipe.read()));
                this.transmissionIndicatorCooldown = 10;
                if(!this.transmissionIndicatorActive)
                    this.setTransmissionIndicator(true);
            }
        }
    }

    // Sets the transmission indicator light and syncs it to client
    private void setTransmissionIndicator(boolean active) {
        this.transmissionIndicatorActive = active;
        CompoundTag tag = new CompoundTag();
        tag.putBoolean("transmissionIndicatorActive", active);
        this.getCasing().sendData(this.getFace(), tag);
    }

    // Sets the packet backlog light and syncs it to client
    private void setQueueIndicatorGreen(boolean empty) {
        this.queueIndicatorGreen = empty;
        CompoundTag tag = new CompoundTag();
        tag.putBoolean("packetQueueEmpty", empty);
        this.getCasing().sendData(this.getFace(), tag);
    }

    private void stepOutput() {
        if (this.packetQueue.isEmpty()) {
            if(!this.queueIndicatorGreen)
                this.setQueueIndicatorGreen(true);
            return;
        }

        for (final Port port : Port.VALUES) {
            final Pipe sendingPipe = this.getCasing().getSendingPipe(this.getFace(), port);
            if (!sendingPipe.isWriting()) {
                sendingPipe.beginWrite(this.packetQueue.peekFirst());
            }
        }
    }

    @Override
    public void onRadioPacket(RadioPacket packet) {
        this.setQueueIndicatorGreen(false);
        if(this.packetQueue.size() < CommonConfig.maxInfraredQueueLength * 4)
            packetQueue.addLast(packet.getData());
    }

    @Override
    public void onEnabled() {
        RadioNetwork.get(this.getCasing().getCasingLevel()).registerRadio(this);
    }

    @Override
    public void onDisabled() {
        this.packetQueue.clear();
        RadioNetwork.get(this.getCasing().getCasingLevel()).deregisterRadio(this);
    }

    @Override
    public void onBeforeWriteComplete(final Port port) {
        // If one completes, cancel all other writes to ensure a value is only
        // written once.
        cancelWrite();
    }

    @Override
    public void onWriteComplete(final Port port) {
        // Re-cancel in case step() was called after onBeforeWriteComplete() to
        // ensure we're not writing while waiting for input.
        cancelWrite();
    }

    @Override
    public Vec3 getPos() {
        BlockPos pos = this.getCasing().getPosition();
        return new Vec3(pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5);
    }

    @Override
    public RadioProvider getProvider() {
        return Provider.INSTANCE;
    }

    @Override
    public void save(CompoundTag tag) {
        super.save(tag);
        tag.putIntArray("packetQueue", 
            this.packetQueue.stream()
                .map((val) -> (int) val)
                .toList()
        );
        tag.putInt("transmissionIndicatorCooldown", transmissionIndicatorCooldown);
    }

    @Override
    public void load(final CompoundTag tag) {
        super.load(tag);
        
        if(!this.getCasing().getCasingLevel().isClientSide()) {
            this.packetQueue = new ArrayDeque<>();
        
            Arrays.stream(tag.getIntArray("packetQueue"))
                .boxed()
                .map(Integer::shortValue)
                .forEachOrdered(this.packetQueue::add);
            this.transmissionIndicatorCooldown = tag.getInt("transmissionIndicatorCooldown");
        }
    }

    @Override
    public void onData(final CompoundTag tag) {
        if(this.getCasing().getCasingLevel().isClientSide()) {
            if(tag.contains("transmissionIndicatorActive"))
                this.transmissionIndicatorActive = tag.getBoolean("transmissionIndicatorActive");

            if(tag.contains("packetQueueEmpty"))
                this.queueIndicatorGreen = tag.getBoolean("packetQueueEmpty");
        }
    }

    @Override
    public void render(RenderContext context) {
        if (!getCasing().isEnabled())
            return;

        final PoseStack matrixStack = context.getMatrixStack();
        matrixStack.pushPose();
        this.rotateForRendering(matrixStack);
        context.drawAtlasQuadUnlit(new ResourceLocation(TISAdvanced.MOD_ID,"block/overlay/radio_module"));

        if (context.closeEnoughForDetails(getCasing().getPosition())) {
            drawState(context);
        }

        matrixStack.popPose();
    }

    private void drawState(RenderContext context) {
        final PoseStack matrixStack = context.getMatrixStack();
        matrixStack.scale(1 / 32f, 1 / 32f, 1);

        int queueIndicatorColor = Color.RED;
        if(this.queueIndicatorGreen) {
            queueIndicatorColor = Color.GREEN;
        }

        int transmissionIndicatorColor = Color.DARK_GRAY;
        if(this.transmissionIndicatorActive) {
            transmissionIndicatorColor = Color.BLUE;
        }

        context.drawQuadUnlit(26, 4, 1, 1, queueIndicatorColor);
        context.drawQuadUnlit(27, 4, 1, 1, transmissionIndicatorColor);
    }

    /*
     * Note: The onEnabled and onDisabled methods from the TIS-3D module interface are called on
     * world load/unload, making this code largely redundant. However, it is left here as an
     * example to guide others in writing providers for their own ratio implementations
     */
    public static class Provider extends ForgeRegistryEntry<RadioProvider> implements RadioProvider {

        private Provider() {}

        public static Provider INSTANCE = new Provider();

        @Override
        public Optional<RadioReceiver> load(Level level, CompoundTag tag) {
            CompoundTag posTag = tag.getCompound("pos");
            BlockPos pos = new BlockPos(posTag.getInt("x"), posTag.getInt("y"), posTag.getInt("z"));

            if(level.getBlockEntity(pos) instanceof Casing casing) {
                Face f = EnumUtils.load(Face.class, "face", tag);
                if(casing.getModule(f) instanceof RadioModule radio) {
                    return Optional.of(radio);
                }
            }

            return Optional.empty();
        }

        @Override
        public void save(RadioReceiver radio, CompoundTag tag) {
            if(radio instanceof RadioModule radioModule) {
                CompoundTag posTag = new CompoundTag();
                posTag.putDouble("x", radioModule.getPos().x);
                posTag.putDouble("y", radioModule.getPos().y);
                posTag.putDouble("z", radioModule.getPos().z);
                tag.put("pos", posTag);
                EnumUtils.save(radioModule.getFace(), "face", tag);
            } else {
                throw new RuntimeException("Attempted to save instance of " + radio.getClass().getName() + " using RadioModule provider");
            }
        }
    }
}
