package cc.emmert.tisadvanced.module.sevensegmentdisplay;

import java.util.BitSet;

import com.mojang.blaze3d.vertex.PoseStack;

import cc.emmert.tisadvanced.TISAdvanced;
import li.cil.tis3d.api.machine.Casing;
import li.cil.tis3d.api.machine.Face;
import li.cil.tis3d.api.machine.Pipe;
import li.cil.tis3d.api.machine.Port;
import li.cil.tis3d.api.prefab.module.AbstractModuleWithRotation;
import li.cil.tis3d.api.util.RenderContext;
import li.cil.tis3d.util.Color;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.DyeItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class SevenSegmentDisplayModule extends AbstractModuleWithRotation {

    private BitSet bits;
    private int color;

    public SevenSegmentDisplayModule(Casing casing, Face face) {
        super(casing, face);
        this.bits = new BitSet(16);
        this.color = Color.RED;
    }

    @Override
    public void step() {
        for(final Port port : Port.VALUES) {
            final Pipe receivingPipe = this.getCasing().getReceivingPipe(this.getFace(), port);
            if (!receivingPipe.isReading()) {
                receivingPipe.beginRead();
            }
            if (receivingPipe.canTransfer()) {
                short input = receivingPipe.read();
                this.processInput(input);
                CompoundTag data = new CompoundTag();
                this.save(data);
                this.getCasing().sendData(this.getFace(), data);
            }
        }
    }
    
    private void processInput(short value) {
        for(int i = 15; i >= 0; i--) {
            int bit = value & (1 << i);
            if(bit == 0) {
                this.bits.clear(15-i);
            } else {
                this.bits.set(15-i);
            }
        }
    }

    public boolean use(Player player, InteractionHand hand, Vec3 hit) {
        ItemStack stack = player.getItemInHand(hand);

        if(stack.getItem() instanceof DyeItem dye) {
            this.color = dyeToColor(dye);
            return true;
        }

        return false;
    }

    private static int dyeToColor(DyeItem dye) {
        return switch(dye.getDyeColor()) {
            case BLACK -> Color.BLACK;
            case BLUE -> Color.BLUE;
            case BROWN -> Color.BROWN;
            case CYAN -> Color.CYAN;
            case GRAY -> Color.GRAY;
            case GREEN -> Color.GREEN;
            case LIGHT_BLUE -> Color.LIGHT_BLUE;
            case LIGHT_GRAY -> Color.LIGHT_GRAY;
            case LIME -> Color.LIME;
            case MAGENTA -> Color.MAGENTA;
            case ORANGE -> Color.ORANGE;
            case PINK -> Color.PINK;
            case PURPLE -> Color.PURPLE;
            case RED -> Color.RED;
            case WHITE -> Color.WHITE;
            case YELLOW -> Color.YELLOW;
        };
    }

    @Override
    public void onDisabled() {
        this.bits.clear();
        CompoundTag data = new CompoundTag();
        this.save(data);
        this.getCasing().sendData(this.getFace(), data);
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public void render(final RenderContext context) {
        if (!getCasing().isEnabled())
            return;

        final PoseStack matrixStack = context.getMatrixStack();
        matrixStack.pushPose();
        this.rotateForRendering(matrixStack);
        context.drawAtlasQuadUnlit(new ResourceLocation(TISAdvanced.MOD_ID,"block/overlay/seven_segment_display"));

        if (context.closeEnoughForDetails(getCasing().getPosition())) {
            drawState(context);
        }

        matrixStack.popPose();
    }

    @Override
    public void onData(CompoundTag data) {
        this.load(data);
    }

    @OnlyIn(Dist.CLIENT)
    private void drawState(RenderContext context) {
        final PoseStack matrixStack = context.getMatrixStack();
        matrixStack.scale(1 / 32f, 1 / 32f, 1);
        
        // Draw the first digit
        this.drawDigit(context, 0, 7, color);

        // Draw the dots
        if(this.bits.get(7)) {
            context.drawQuadUnlit(15, 10, 1, 1, color);
        }
        if(this.bits.get(8)) {
            context.drawQuadUnlit(15, 22, 1, 1, color);
        }

        // Draw the second digit
        this.drawDigit(context, 9, 17, color);
    }

    @OnlyIn(Dist.CLIENT)
    private void drawDigit(RenderContext context, int bitOffset, float xOffset, int color) {

        if(this.bits.get(bitOffset)) {
            drawHorizontalSegment(context, color, xOffset+1, 10);
        }
        if(this.bits.get(bitOffset+1)) {
            drawVerticalSegment(context, color, xOffset+6, 11);
        }
        if(this.bits.get(bitOffset+2)) {
            drawVerticalSegment(context, color, xOffset+6, 17);
        }
        if(this.bits.get(bitOffset+3)) {
            drawHorizontalSegment(context, color, xOffset+1, 22);
        }
        if(this.bits.get(bitOffset+4)) {
            drawVerticalSegment(context, color, xOffset, 17);
        }
        if(this.bits.get(bitOffset+5)) {
            drawVerticalSegment(context, color, xOffset, 11);
        }
        if(this.bits.get(bitOffset+6)) {
            drawHorizontalSegment(context, color, xOffset+1, 16);
        }
    }

    @OnlyIn(Dist.CLIENT)
    private static void drawHorizontalSegment(RenderContext context, int color, float xPos, float yPos) {
        context.drawQuadUnlit(xPos, yPos, 5, 1, color);
    }

    @OnlyIn(Dist.CLIENT)
    private static void drawVerticalSegment(RenderContext context, int color, float xPos, float yPos) {
        context.drawQuadUnlit(xPos, yPos, 1, 5, color);
    }

    @Override
    public void load(final CompoundTag tag) {
        super.load(tag);
        byte[] bitsAsBytes = tag.getByteArray("bits");
        this.readFromByteArray(bitsAsBytes);
        this.color = tag.getInt("color");
    }

    @Override
    public void save(final CompoundTag tag) {
        super.save(tag);
        byte[] bitsAsBytes = this.bits.toByteArray();
        
        tag.putByteArray("bits", bitsAsBytes);
        tag.putInt("color", color);
    }

    private void readFromByteArray(byte[] bytes) {
        this.bits = BitSet.valueOf(bytes);
    }
}
