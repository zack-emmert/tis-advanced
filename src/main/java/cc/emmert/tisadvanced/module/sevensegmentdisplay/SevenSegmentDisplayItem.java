package cc.emmert.tisadvanced.module.sevensegmentdisplay;

import li.cil.tis3d.api.API;
import li.cil.tis3d.common.item.ModuleItem;

public class SevenSegmentDisplayItem extends ModuleItem {

    public SevenSegmentDisplayItem() {
        super(createProperties().tab(API.itemGroup));
    }
}
