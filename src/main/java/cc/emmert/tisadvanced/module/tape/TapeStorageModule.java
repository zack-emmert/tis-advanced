package cc.emmert.tisadvanced.module.tape;

import com.mojang.blaze3d.vertex.PoseStack;

import cc.emmert.tisadvanced.TISAdvanced;
import li.cil.tis3d.api.machine.Casing;
import li.cil.tis3d.api.machine.Face;
import li.cil.tis3d.api.machine.Pipe;
import li.cil.tis3d.api.machine.Port;
import li.cil.tis3d.api.prefab.module.AbstractModuleWithRotation;
import li.cil.tis3d.api.util.RenderContext;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class TapeStorageModule extends AbstractModuleWithRotation {

    private static int MEM_SIZE = 8192;

    private byte[] data;
    private int cursor;
    private int cooldown;

    public TapeStorageModule(Casing casing, Face face) {
        super(casing, face);
        this.data = new byte[MEM_SIZE];
        this.cooldown = 0;
    }
    
    @Override
    public void step() {
        if(this.cooldown == 0) {
            this.stepInput();
            this.stepOutput();
        } else {
            this.cooldown--;
        }
    }

    private void stepOutput() {
        this.cancelWrite();
        for (final Port port : Port.VALUES) {
            final Pipe sendingPipe = getCasing().getSendingPipe(getFace(), port);
            if (!sendingPipe.isWriting()) {
                sendingPipe.beginWrite((short) this.get());
            }
        }
    }

    private int get() {
        return this.data[this.cursor] & 0xFF;
    }

    private void stepInput() {
        for(final Port port : Port.VALUES) {
            final Pipe receivingPipe = this.getCasing().getReceivingPipe(this.getFace(), port);
            if (!receivingPipe.isReading()) {
                receivingPipe.beginRead();
            }
            if (receivingPipe.canTransfer()) {
                short input = receivingPipe.read();

                short controlCode = (short) ((input & 0xFF00) >> 8); // High 8 bits are control code
                byte value = (byte) (input & 0xFF); // Low 8 bits are value to write

                switch(controlCode) {
                    case 0 -> {
                        this.data[this.cursor] = value;
                    }
                    case 1 -> {
                        this.cursor++;
                    }
                    case 2 -> {
                        this.cursor--;
                    }
                    case 3 -> {
                        this.cursor += value;
                        this.cooldown = value / 128;
                    }
                    case 4 -> {
                        this.cursor -= value;
                        this.cooldown = value / 128;
                    }
                    case 5 -> {
                        this.cooldown = this.cursor / 128;
                        this.cursor = 0;
                    }
                    case 6 -> {
                        this.cooldown = (MEM_SIZE - this.cursor) / 128;
                        this.cursor = MEM_SIZE - 1;
                    }
                }

                if(this.cursor >= MEM_SIZE)
                    this.cursor = MEM_SIZE - 1;
                else if(this.cursor < 0) {
                    this.cursor = 0;
                }
            }
        }
    }

    @OnlyIn(Dist.CLIENT)
    public void render(final RenderContext context) {
        final PoseStack matrixStack = context.getMatrixStack();
        matrixStack.pushPose();
        this.rotateForRendering(matrixStack);
        context.drawAtlasQuadUnlit(new ResourceLocation(TISAdvanced.MOD_ID,"block/overlay/tape_storage"));
        matrixStack.popPose();
    }

    @Override
    public void onInstalled(final ItemStack stack) {
        this.load(stack.getTag());
    }

    @Override
    public void onUninstalled(final ItemStack stack) {
        this.save(stack.getOrCreateTag());
    }

    @Override
    public void onBeforeWriteComplete(final Port port) {
        // If one completes, cancel all other writes to ensure a value is only
        // written once.
        cancelWrite();
    }

    @Override
    public void onWriteComplete(final Port port) {
        // Re-cancel in case step() was called after onBeforeWriteComplete() to
        // ensure we're not writing while waiting for input.
        cancelWrite();
    }

    @Override
    public void load(final CompoundTag tag) {
        if(tag != null) {
            super.load(tag);
            this.loadFromTag(tag);
        } else {
            this.cursor = 0;
            this.data = new byte[MEM_SIZE];
            this.cooldown = 0;
        }
        
    }

    @Override
    public void save(final CompoundTag tag) {
        super.save(tag);
        this.saveToTag(tag);
    }

    private void saveToTag(CompoundTag tag) {
        tag.putInt("cursor", this.cursor);
        tag.putByteArray("data", this.data);
        tag.putInt("cooldown", this.cooldown);
    }

    private void loadFromTag(CompoundTag tag) {
        this.cursor = tag.getInt("cursor");
        this.data = tag.getByteArray("data");
        this.cooldown = tag.getInt("cooldown");
    }

}
