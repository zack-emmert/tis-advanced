package cc.emmert.tisadvanced.module.tape;

import li.cil.tis3d.api.API;
import li.cil.tis3d.common.item.ModuleItem;

public class TapeStorageModuleItem extends ModuleItem {

    public TapeStorageModuleItem() {
        super(createProperties().stacksTo(1).tab(API.itemGroup));
    }
}
