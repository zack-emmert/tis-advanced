package cc.emmert.tisadvanced.module.radar;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.mojang.blaze3d.vertex.PoseStack;

import cc.emmert.tisadvanced.TISAdvanced;
import cc.emmert.tisadvanced.util.HalfFloat;
import li.cil.manual.api.render.FontRenderer;
import li.cil.tis3d.api.API;
import li.cil.tis3d.api.machine.Casing;
import li.cil.tis3d.api.machine.Face;
import li.cil.tis3d.api.machine.Pipe;
import li.cil.tis3d.api.machine.Port;
import li.cil.tis3d.api.prefab.module.AbstractModuleWithRotation;
import li.cil.tis3d.api.util.RenderContext;
import li.cil.tis3d.util.Color;
import li.cil.tis3d.util.EnumUtils;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction.Axis;
import net.minecraft.world.phys.Vec3;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.FlyingMob;
import net.minecraft.world.entity.ambient.AmbientCreature;
import net.minecraft.world.entity.animal.AbstractGolem;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.animal.WaterAnimal;
import net.minecraft.world.entity.boss.enderdragon.EnderDragon;
import net.minecraft.world.entity.item.FallingBlockEntity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.item.PrimedTnt;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.monster.Shulker;
import net.minecraft.world.entity.monster.Slime;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.entity.vehicle.AbstractMinecart;
import net.minecraft.world.entity.vehicle.Boat;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class RadarModule extends AbstractModuleWithRotation {

    static enum ENTITY_FILTER {
        NONE,
        PLAYER,
        ANIMAL,
        GOLEM,
        MONSTER,
        VEHICLE,
        PROJ,
        ITEM,
        BLOCK,
        OTHER;

        public Predicate<Entity> getPredicate() {
            return switch(this) {
                case NONE -> (entity) -> true;
                case PLAYER -> (entity) -> entity instanceof Player;
                case ANIMAL -> (entity) -> entity instanceof Animal || entity instanceof WaterAnimal || entity instanceof AmbientCreature;
                case GOLEM -> (entity) -> entity instanceof AbstractGolem && !(entity instanceof Shulker);
                case MONSTER -> (entity) -> entity instanceof Monster || entity instanceof Shulker || entity instanceof FlyingMob || entity instanceof Slime || entity instanceof EnderDragon;
                case VEHICLE -> (entity) -> entity instanceof AbstractMinecart || entity instanceof Boat;
                case PROJ -> (entity) -> entity instanceof Projectile;
                case ITEM -> (entity) -> entity instanceof ItemEntity;
                case BLOCK -> (entity) -> entity instanceof FallingBlockEntity || entity instanceof PrimedTnt;
                
                // Return a predicate that filters *out* anything that any other predicate would pass.
                case OTHER -> 
                    (entity) -> 
                        !ENTITY_FILTER.PLAYER.getPredicate().test(entity) && 
                        !ENTITY_FILTER.ANIMAL.getPredicate().test(entity) &&
                        !ENTITY_FILTER.GOLEM.getPredicate().test(entity) &&
                        !ENTITY_FILTER.MONSTER.getPredicate().test(entity) &&
                        !ENTITY_FILTER.VEHICLE.getPredicate().test(entity) &&
                        !ENTITY_FILTER.PROJ.getPredicate().test(entity) &&
                        !ENTITY_FILTER.ITEM.getPredicate().test(entity) &&
                        !ENTITY_FILTER.BLOCK.getPredicate().test(entity);
            };
        }

        public ENTITY_FILTER next() {
            if(this.ordinal() == ENTITY_FILTER.values().length - 1) {
                return ENTITY_FILTER.values()[0];
            } else {
                return ENTITY_FILTER.values()[this.ordinal() + 1];
            }
        }

        public ENTITY_FILTER prev() {
            if(this.ordinal() == 0) {
                return ENTITY_FILTER.values()[ENTITY_FILTER.values().length - 1];
            } else {
                return ENTITY_FILTER.values()[this.ordinal() - 1];
            }
        }
    }

    private static final int AXIS_MASK = 0x6000;
    private static final int REVERSE_INDEX_MASK = 0x8000;
    private static final int INDEX_MASK = 0x1FFF;

    private Optional<Axis> offsetAxis;
    private ENTITY_FILTER entityFilter;
    private int index;
    private boolean reverseIndex;

    // Synced to client to control indicator LED
    private boolean entitiesFound;

    public RadarModule(Casing casing, Face face) {
        super(casing, face);
        this.offsetAxis = Optional.empty();
        this.entityFilter = ENTITY_FILTER.NONE;
        this.reverseIndex = false;
        this.index = 0;
        this.entitiesFound = false;
    }

    @Override
    public void step() {
        this.stepInput();
        this.stepOutput();
    }

    private void stepInput() {
        for(final Port port : Port.VALUES) {
            final Pipe receivingPipe = this.getCasing().getReceivingPipe(this.getFace(), port);
            if (!receivingPipe.isReading()) {
                receivingPipe.beginRead();
            }
            if (receivingPipe.canTransfer()) {
                short input = receivingPipe.read();

                this.reverseIndex = (input & REVERSE_INDEX_MASK) != 0;
                this.offsetAxis = switch ((input & AXIS_MASK) >> 13) {
                    case 1 -> Optional.of(Axis.X);
                    case 2 -> Optional.of(Axis.Y);
                    case 3 -> Optional.of(Axis.Z);
                    default -> Optional.empty();
                };

                this.index = (input & INDEX_MASK);
            }

        }
    }

    private void stepOutput() {
        this.cancelWrite();
        for (final Port port : Port.VALUES) {
            final Pipe sendingPipe = getCasing().getSendingPipe(getFace(), port);
            if (!sendingPipe.isWriting()) {
                List<Double> matchingEntitiesData = this.getMatchingEntitiesData();
                double output;

                this.setEntitiesFound(!matchingEntitiesData.isEmpty());

                // Bounds check: If the player wants an entity at an index that isn't there, vomit up a NaN because an HCF is too harsh.
                if(this.index >= matchingEntitiesData.size()) {
                    sendingPipe.beginWrite(HalfFloat.NaN);
                    return;
                }

                if(this.reverseIndex) {
                    output = matchingEntitiesData.get(matchingEntitiesData.size()-this.index);
                } else {
                    output = matchingEntitiesData.get(this.index);
                }

                sendingPipe.beginWrite(HalfFloat.toHalf((float) output));
            }
        }
    }

    private List<Double> getMatchingEntitiesData() {
        BlockPos pos = this.getCasing().getPosition();
        List<Entity> entities = this.getCasing().getCasingLevel().getEntities(
            null, 
            new AABB(0,0,0,0,0,0).move(pos).inflate(256)
        );

        entities.sort((e1,e2) -> Double.compare(distanceBetween(pos, e1),distanceBetween(pos, e2)));

        List<Double> entityData = entities.stream()
            .filter(this.entityFilter.getPredicate())
            .map((entity) -> {
                if(this.offsetAxis.isPresent()) {
                    Vec3 offset = relativePosition(pos, entity);
                    double output = 0.0;
                    switch(this.offsetAxis.get()) {
                        case X -> output = offset.x;
                        case Y -> output = offset.y;
                        case Z -> output = offset.z;
                    }
                    return output;
                } else {
                    return distanceBetween(pos, entity);
                }
            })
            .collect(Collectors.toList());
        return entityData;
    }

    private static Vec3 relativePosition(BlockPos pos, Entity entity) {
        return new Vec3(entity.getX() - pos.getX() + 0.5, entity.getY() - pos.getY() + 0.5, entity.getZ() - pos.getZ() + 0.5);
    }

    private static double distanceBetween(BlockPos pos, Entity entity) {
        Vec3 rPos = relativePosition(pos, entity);
        return Math.sqrt(Math.pow(rPos.x, 2) + Math.pow(rPos.y,2) + Math.pow(rPos.z,2));
    }

    private void setEntitiesFound(boolean value) {
        this.entitiesFound = value;
        CompoundTag tag = new CompoundTag();
        tag.putBoolean("entitiesFound", value);
        this.getCasing().sendData(this.getFace(), tag);
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        if(tag.contains("offsetAxis"))
            this.offsetAxis = Optional.of(EnumUtils.load(Axis.class, "offsetAxis", tag));
        else
            this.offsetAxis = Optional.empty();

        this.entityFilter = EnumUtils.load(ENTITY_FILTER.class, "entityFilter", tag);
        this.reverseIndex = tag.getBoolean("reverseIndex");
        this.index = tag.getInt("index");
    }

    @Override
    public void save(CompoundTag tag) {
        super.save(tag);
        this.offsetAxis.ifPresent((axis) -> 
            EnumUtils.save(axis, "offsetAxis", tag)
        );
        EnumUtils.save(this.entityFilter, "entityFilter", tag);
        tag.putBoolean("reverseIndex", this.reverseIndex);
        tag.putInt("index", this.index);
    }

    @Override
    public void onBeforeWriteComplete(final Port port) {
        // If one completes, cancel all other writes to ensure a value is only
        // written once.
        this.cancelWrite();
    }

    @Override
    public void onWriteComplete(final Port port) {
        // Re-cancel in case step() was called after onBeforeWriteComplete() to
        // ensure we're not writing while waiting for input.
        this.cancelWrite();
    }

    @Override
    public void onData(CompoundTag data) {
        if(data.contains("entitiesFound")) {
            this.entitiesFound = data.getBoolean("entitiesFound");
        }
    }

    @Override
    public void onInstalled(final ItemStack stack) {
        this.entityFilter = RadarModuleItem.loadFromStack(stack);
    }

    @Override
    public void onUninstalled(final ItemStack stack) {
        RadarModuleItem.saveToStack(stack, this.entityFilter);
    }

    @Override
    public void onDisabled() {
        this.offsetAxis = Optional.empty();
        this.reverseIndex = false;
        this.index = 0;
    }
    
    @OnlyIn(Dist.CLIENT)
    public void render(final RenderContext context) {

        final PoseStack matrixStack = context.getMatrixStack();
        matrixStack.pushPose();
        this.rotateForRendering(matrixStack);
        context.drawAtlasQuadUnlit(new ResourceLocation(TISAdvanced.MOD_ID,"block/overlay/radar_module"));

        if (context.closeEnoughForDetails(getCasing().getPosition())) {
            drawState(context);
        }

        matrixStack.popPose();
    }

    @OnlyIn(Dist.CLIENT)
    private void drawState(RenderContext context) {
        final PoseStack matrixStack = context.getMatrixStack();
        matrixStack.pushPose();
        final FontRenderer font = API.normalFontRenderer;

        matrixStack.translate(3 / 16f, 5 / 16f, 0);
        matrixStack.scale(1 / 96f, 1 / 96f, 1);

        // Brute-force centering of the filter name string
        switch(this.entityFilter.name().length()) {
            case 7 -> matrixStack.translate(0.0f, 10f, 0);
            case 6 -> matrixStack.translate(3.5f, 10f, 0);
            case 5 -> matrixStack.translate(7.25f, 10f, 0);
            case 4 -> matrixStack.translate(12.5f, 10f, 0);
        }

        context.drawString(font, this.entityFilter.name(), Color.WHITE);

        matrixStack.popPose();
        matrixStack.scale(1 / 32f, 1 / 32f, 1 / 32f);

        int indicatorColor;
        if(this.entitiesFound) {
            indicatorColor = Color.GREEN;
        } else {
            indicatorColor = Color.RED;
        }

        context.drawQuadUnlit(24, 6, 2, 2, indicatorColor);
    }
}
