package cc.emmert.tisadvanced.module.radar;

import java.util.List;

import javax.annotation.Nullable;

import cc.emmert.tisadvanced.module.radar.RadarModule.ENTITY_FILTER;
import li.cil.tis3d.api.API;
import li.cil.tis3d.common.item.ModuleItem;
import li.cil.tis3d.util.EnumUtils;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class RadarModuleItem extends ModuleItem {

    public RadarModuleItem() {
        super(createProperties().stacksTo(1).tab(API.itemGroup));
    }
    
    @Override
    public InteractionResultHolder<ItemStack> use(final Level level, final Player player, final InteractionHand hand) {
        ItemStack stack = player.getItemInHand(hand);
        ENTITY_FILTER filter = loadFromStack(stack);

        ENTITY_FILTER nextMode;
        if(player.isShiftKeyDown()) {
            nextMode = filter.prev();
        } else {
            nextMode = filter.next();
        }

        saveToStack(stack, nextMode);
        if(level.isClientSide()) {
            player.displayClientMessage(new TextComponent(nextMode.name()), true);
        }
        return InteractionResultHolder.sidedSuccess(player.getItemInHand(hand), level.isClientSide());
    }

    @Override
    public void appendHoverText(ItemStack stack, @Nullable Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        ENTITY_FILTER filter = loadFromStack(stack);

        tooltip.add(new TextComponent("Filter: " + filter.toString()));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    public static ENTITY_FILTER loadFromTag(@Nullable final CompoundTag tag) {
        if (tag != null) {
            return EnumUtils.load(ENTITY_FILTER.class, "entityFilter", tag);
        } else {
            return ENTITY_FILTER.PLAYER;
        }
    }

    public static ENTITY_FILTER loadFromStack(final ItemStack stack) {
        return loadFromTag(stack.getTag());
    }

    public static void saveToStack(final ItemStack stack, final ENTITY_FILTER filter) {
        final CompoundTag tag = stack.getOrCreateTag();
        EnumUtils.save(filter, "entityFilter", tag);
    }
}
