package cc.emmert.tisadvanced.mixin;

import java.util.Map;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import cc.emmert.tisadvanced.util.HalfFloat;
import li.cil.tis3d.common.module.execution.compiler.ParseException;
import li.cil.tis3d.common.module.execution.compiler.Strings;
import li.cil.tis3d.common.module.execution.compiler.instruction.MoveInstructionEmitter;
import li.cil.tis3d.common.module.execution.target.Target;

@Mixin(MoveInstructionEmitter.class)
public class MixinMoveInstructionEmitter {
    @Redirect(method = "compile", remap = false, at = @At(value = "INVOKE", target = "Lli/cil/tis3d/common/module/execution/compiler/instruction/MoveInstructionEmitter;checkTargetOrNumber(Ljava/lang/String;ILjava/util/Map;II)Ljava/lang/Object;"))
    private Object moveInstructionEmitterModifier(String name, final int lineNumber, final Map<String, String> defines, final int start, final int end) throws ParseException {
        name = defines.getOrDefault(name, name);
        try {
            final Target target = Enum.valueOf(Target.class, name);
            if (!Target.VALID_TARGETS.contains(target)) {
                throw new ParseException(Strings.MESSAGE_PARAMETER_INVALID, lineNumber, start, end);
            }
            return target;
        } catch (final IllegalArgumentException ex) {
            try {
                return Integer.decode(name).shortValue();
            } catch (final NumberFormatException ignored) {
                // If we can't parse it as an int, try parsing as a float
                try {
                    return HalfFloat.toHalf(Float.parseFloat(name));
                } catch (final NumberFormatException ignored2) {
                    throw new ParseException(Strings.MESSAGE_PARAMETER_INVALID, lineNumber, start, end);
                }
            }
        }
    }
}
