package cc.emmert.tisadvanced.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

import com.google.common.collect.ImmutableMap;
import com.llamalad7.mixinextras.injector.ModifyReceiver;

import cc.emmert.tisadvanced.instruction.arithmetic.*;
import cc.emmert.tisadvanced.instruction.conditional.*;
import cc.emmert.tisadvanced.instruction.conversion.*;
import cc.emmert.tisadvanced.instruction.emitter.FloatMathInstructionEmitter;
import li.cil.tis3d.common.module.execution.compiler.Compiler;
import li.cil.tis3d.common.module.execution.compiler.instruction.InstructionEmitter;
import li.cil.tis3d.common.module.execution.compiler.instruction.LabelInstructionEmitter;
import li.cil.tis3d.common.module.execution.compiler.instruction.UnaryInstructionEmitter;

@Mixin(Compiler.class)
public class MixinCompiler {
    @ModifyReceiver(
            method = "<clinit>",
            at = @At(value = "INVOKE", target = "Lcom/google/common/collect/ImmutableMap$Builder;build()Lcom/google/common/collect/ImmutableMap;")
    )
    private static @SuppressWarnings("all") ImmutableMap.Builder<String,InstructionEmitter> instructionMapModifier(ImmutableMap.Builder<String,InstructionEmitter> builder) {
        // Floating point arithmetic operations
        builder.put(FloatAddInstruction.NAME, new FloatMathInstructionEmitter(FloatAddInstruction::new, FloatAddImmediateInstruction::new));
        builder.put(FloatSubInstruction.NAME, new FloatMathInstructionEmitter(FloatSubInstruction::new, FloatSubImmediateInstruction::new));
        builder.put(FloatMulInstruction.NAME, new FloatMathInstructionEmitter(FloatMulInstruction::new, FloatMulImmediateInstruction::new));
        builder.put(FloatDivInstruction.NAME, new FloatMathInstructionEmitter(FloatDivInstruction::new, FloatDivImmediateInstruction::new));

        // Floating point control flow
        builder.put(FloatJumpEqualZeroInstruction.NAME, new LabelInstructionEmitter(FloatJumpEqualZeroInstruction::new));
        builder.put(FloatJumpNotZeroInstruction.NAME, new LabelInstructionEmitter(FloatJumpNotZeroInstruction::new));
        builder.put(FloatJumpGreaterThanZeroInstruction.NAME, new LabelInstructionEmitter(FloatJumpGreaterThanZeroInstruction::new));
        builder.put(FloatJumpLessThanZeroInstruction.NAME, new LabelInstructionEmitter(FloatJumpLessThanZeroInstruction::new));
        builder.put(FloatJumpNanInstruction.NAME, new LabelInstructionEmitter(FloatJumpNanInstruction::new));
        builder.put(FloatJumpNotNanInstruction.NAME, new LabelInstructionEmitter(FloatJumpNotNanInstruction::new));

        // Float <-> Int conversion operations
        builder.put(IntToFloatInstruction.NAME, new UnaryInstructionEmitter(() -> IntToFloatInstruction.INSTANCE));
        builder.put(FloatToIntInstruction.NAME, new UnaryInstructionEmitter(() -> FloatToIntInstruction.INSTANCE));
        return builder;
    };
}
