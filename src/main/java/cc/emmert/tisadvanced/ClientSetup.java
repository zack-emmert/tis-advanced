package cc.emmert.tisadvanced;

import java.util.Objects;

import cc.emmert.tisadvanced.manual.TISAdvancedContentProvider;
import cc.emmert.tisadvanced.manual.TISAdvancedPathProvider;
import cc.emmert.tisadvanced.manual.TISAdvancedTab;
import li.cil.manual.api.ManualModel;
import li.cil.manual.api.Tab;
import li.cil.manual.api.provider.DocumentProvider;
import li.cil.manual.api.provider.PathProvider;
import li.cil.manual.api.util.Constants;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.inventory.InventoryMenu;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;

@OnlyIn(Dist.CLIENT)
public class ClientSetup {

    static final DeferredRegister<Tab> TABS = DeferredRegister.create(Constants.TAB_REGISTRY, TISAdvanced.MOD_ID);
    static final DeferredRegister<PathProvider> PATH_PROVIDERS = DeferredRegister.create(Constants.PATH_PROVIDER_REGISTRY, TISAdvanced.MOD_ID);
    static final DeferredRegister<DocumentProvider> CONTENT_PROVIDERS = DeferredRegister.create(Constants.DOCUMENT_PROVIDER_REGISTRY, TISAdvanced.MOD_ID);

    static void initialize() {
        CONTENT_PROVIDERS.register("content_provider", () -> new TISAdvancedContentProvider(TISAdvanced.MOD_ID,"doc"));
        PATH_PROVIDERS.register("path_provider", () -> new TISAdvancedPathProvider(TISAdvanced.MOD_ID));
        TABS.register(TISAdvanced.MOD_ID, () -> {
            return new TISAdvancedTab(ManualModel.LANGUAGE_KEY + "/tisadvanced.md", new TranslatableComponent("tisadvanced.manual.tab"));
        });

        CONTENT_PROVIDERS.register(FMLJavaModLoadingContext.get().getModEventBus());
        PATH_PROVIDERS.register(FMLJavaModLoadingContext.get().getModEventBus());
        TABS.register(FMLJavaModLoadingContext.get().getModEventBus());
    }

    @SubscribeEvent
    public static void handleTextureStitchEvent(final TextureStitchEvent.Pre event) {
        if (Objects.equals(event.getAtlas().location(), InventoryMenu.BLOCK_ATLAS)) {
            event.addSprite(new ResourceLocation(TISAdvanced.MOD_ID,"block/overlay/asic_module"));
            event.addSprite(new ResourceLocation(TISAdvanced.MOD_ID,"block/overlay/tape_storage"));
            event.addSprite(new ResourceLocation(TISAdvanced.MOD_ID,"block/overlay/seven_segment_display"));
            event.addSprite(new ResourceLocation(TISAdvanced.MOD_ID,"block/overlay/radio_module"));
            event.addSprite(new ResourceLocation(TISAdvanced.MOD_ID,"block/overlay/radar_module"));
        }
    }
}
