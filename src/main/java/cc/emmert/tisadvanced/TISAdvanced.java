package cc.emmert.tisadvanced;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cc.emmert.tisadvanced.module.asic.*;
import cc.emmert.tisadvanced.module.radar.RadarModule;
import cc.emmert.tisadvanced.module.radar.RadarModuleItem;
import cc.emmert.tisadvanced.module.radio.*;
import cc.emmert.tisadvanced.module.sevensegmentdisplay.*;
import cc.emmert.tisadvanced.module.tape.*;
import li.cil.tis3d.api.module.ModuleProvider;
import li.cil.tis3d.common.provider.module.SimpleModuleProvider;
import net.minecraft.world.item.Item;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

@Mod("tisadvanced")
public class TISAdvanced {

    public static final String MOD_ID = "tisadvanced";
    public static final Logger LOGGER = LogManager.getLogger(MOD_ID);

    static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, MOD_ID);
    static final DeferredRegister<ModuleProvider> MODULES = DeferredRegister.create(ModuleProvider.REGISTRY, MOD_ID);

    public static final RegistryObject<ASICModuleItem> ASIC_ITEM = ITEMS.register("asic_module", ASICModuleItem::new);
    public static final RegistryObject<TapeStorageModuleItem> TAPE_STORAGE_ITEM = ITEMS.register("tape_storage", TapeStorageModuleItem::new);
    public static final RegistryObject<RadioModuleItem> RADIO_ITEM = ITEMS.register("radio_module", RadioModuleItem::new);
    public static final RegistryObject<SevenSegmentDisplayItem> SEVEN_SEG_DISPLAY_ITEM = ITEMS.register("seven_segment_display", SevenSegmentDisplayItem::new);
    public static final RegistryObject<RadarModuleItem> RADAR_ITEM = ITEMS.register("radar_module", RadarModuleItem::new);

    public TISAdvanced() {
        
        MODULES.register("asic_module", () -> new SimpleModuleProvider<>(ASIC_ITEM, ASICModule::new));
        MODULES.register("tape_storage", () -> new SimpleModuleProvider<>(TAPE_STORAGE_ITEM, TapeStorageModule::new));
        MODULES.register("radio", () -> new SimpleModuleProvider<>(RADIO_ITEM, RadioModule::new));
        MODULES.register("seven_segment_display", () -> new SimpleModuleProvider<>(SEVEN_SEG_DISPLAY_ITEM, SevenSegmentDisplayModule::new));
        MODULES.register("radar", () -> new SimpleModuleProvider<>(RADAR_ITEM, RadarModule::new));
        RadioNetwork.RADIOS.register("radio_module", () -> RadioModule.Provider.INSTANCE);
        
        MODULES.register(FMLJavaModLoadingContext.get().getModEventBus());
        ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
        RadioNetwork.RADIOS.register(FMLJavaModLoadingContext.get().getModEventBus());

        DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> ClientSetup::initialize);

        MinecraftForge.EVENT_BUS.register(this);
        DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> {
            FMLJavaModLoadingContext.get().getModEventBus().register(ClientSetup.class);
        });
    }
}
