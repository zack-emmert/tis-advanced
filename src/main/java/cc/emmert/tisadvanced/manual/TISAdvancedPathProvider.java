package cc.emmert.tisadvanced.manual;

import java.util.Objects;

import li.cil.manual.api.ManualModel;
import li.cil.manual.api.prefab.provider.NamespacePathProvider;
import li.cil.tis3d.client.manual.Manuals;

public class TISAdvancedPathProvider extends NamespacePathProvider {

    public TISAdvancedPathProvider(String namespace) {
        this(namespace, false);
    }

    public TISAdvancedPathProvider(String namespace, boolean keepNamespaceInPath) {
        super(namespace, keepNamespaceInPath);
    }

    @Override
    public boolean matches(ManualModel manual) {
        return Objects.equals(manual, Manuals.MANUAL.get());
    }

    @Override
    public int sortOrder() {
        return Integer.MAX_VALUE;
    }
}
