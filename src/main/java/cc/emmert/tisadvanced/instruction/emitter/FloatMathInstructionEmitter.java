package cc.emmert.tisadvanced.instruction.emitter;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;

import cc.emmert.tisadvanced.util.HalfFloat;
import li.cil.tis3d.common.module.execution.compiler.ParseException;
import li.cil.tis3d.common.module.execution.compiler.Strings;
import li.cil.tis3d.common.module.execution.compiler.Validator;
import li.cil.tis3d.common.module.execution.compiler.instruction.InstructionEmitter;
import li.cil.tis3d.common.module.execution.instruction.Instruction;
import li.cil.tis3d.common.module.execution.target.Target;

public class FloatMathInstructionEmitter implements InstructionEmitter {

    private final Function<Target,Instruction> constructorTarget;
    private final Function<Short,Instruction> constructorImmediate;

    public FloatMathInstructionEmitter(Function<Target,Instruction> constructorTarget, Function<Short,Instruction> constructorImmediate) {
        this.constructorTarget = constructorTarget;
        this.constructorImmediate = constructorImmediate;
    }

    @Override
    public Instruction compile(Matcher matcher, int lineNumber, Map<String, String> defines, List<Validator> validators) throws ParseException {
        final Object src = checkTargetOrNumber(checkArg(lineNumber, matcher, "arg1", "name"),
            lineNumber, defines, matcher.start("arg1"), matcher.end("arg1"));
        checkExcess(lineNumber, matcher, "arg2");

        if (src instanceof Target) {
            return constructorTarget.apply((Target) src);
        } else /* if (src instanceof Integer) */ {
            return constructorImmediate.apply((Short) src);
        }
    }

    private static void checkExcess(int lineNumber, Matcher matcher, String name) throws ParseException {
        final int start = matcher.start(name);
        if (start >= 0) {
            throw new ParseException(Strings.MESSAGE_PARAMETER_OVERFLOW, lineNumber, start, matcher.end());
        }
    }

    private static Object checkTargetOrNumber(String name, final int lineNumber, final Map<String, String> defines, final int start, final int end) throws ParseException {
        name = defines.getOrDefault(name, name);
        try {
            final Target target = Enum.valueOf(Target.class, name);
            if (!Target.VALID_TARGETS.contains(target)) {
                throw new ParseException(Strings.MESSAGE_PARAMETER_INVALID, lineNumber, start, end);
            }
            return target;
        } catch (final IllegalArgumentException ex) {
            try {
                return HalfFloat.toHalf(Float.parseFloat(name));
            } catch (final NumberFormatException ignored) {
                throw new ParseException(Strings.MESSAGE_PARAMETER_INVALID, lineNumber, start, end);
            }
        }
    }

    private static String checkArg(int lineNumber, Matcher matcher, String name, String previous) throws ParseException {
        final String arg = matcher.group(name);
        if (arg == null) {
            throw new ParseException(Strings.MESSAGE_PARAMETER_UNDERFLOW, lineNumber, matcher.end(previous) + 1, matcher.end(previous) + 1);
        }
        return arg;
    }
    
}
