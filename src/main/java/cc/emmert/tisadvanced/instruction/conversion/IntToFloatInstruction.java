package cc.emmert.tisadvanced.instruction.conversion;

import cc.emmert.tisadvanced.util.HalfFloat;
import cc.emmert.tisadvanced.util.NumUtils;
import li.cil.tis3d.common.module.execution.Machine;
import li.cil.tis3d.common.module.execution.MachineState;
import li.cil.tis3d.common.module.execution.instruction.Instruction;

public class IntToFloatInstruction implements Instruction {
    public static final String NAME = "FLT";
    public static final Instruction INSTANCE = new IntToFloatInstruction();

    @Override
    public void step(final Machine machine) {
        final MachineState state = machine.getState();
        state.acc = HalfFloat.toHalf(NumUtils.clampFloat((float) state.acc));
        state.pc++;
    }
}
