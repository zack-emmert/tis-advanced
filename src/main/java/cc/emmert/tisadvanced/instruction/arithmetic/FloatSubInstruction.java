package cc.emmert.tisadvanced.instruction.arithmetic;

import li.cil.tis3d.common.module.execution.target.Target;

public class FloatSubInstruction extends AbstractFloatMathInstruction {
    public FloatSubInstruction(Target source) {
        super(source);
    }

    public static final String NAME = "SUBF";

    @Override
    float applyOperation(float acc, float value) {
        return acc - value;
    }
    
}