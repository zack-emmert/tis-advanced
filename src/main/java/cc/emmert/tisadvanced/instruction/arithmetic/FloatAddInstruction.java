package cc.emmert.tisadvanced.instruction.arithmetic;

import li.cil.tis3d.common.module.execution.target.Target;

public class FloatAddInstruction extends AbstractFloatMathInstruction {
    public FloatAddInstruction(Target source) {
        super(source);
    }

    public static final String NAME = "ADDF";

    @Override
    float applyOperation(float acc, float value) {
        return acc + value;
    }
    
}