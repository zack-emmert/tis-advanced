package cc.emmert.tisadvanced.instruction.arithmetic;

public class FloatMulImmediateInstruction extends AbstractFloatMathImmediateInstruction {

    public FloatMulImmediateInstruction(short value) {
        super(value);
    }

    @Override
    float applyOperation(float acc, float value) {
        return acc * value;
    }
    
}