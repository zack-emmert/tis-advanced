package cc.emmert.tisadvanced.instruction.arithmetic;

public class FloatSubImmediateInstruction extends AbstractFloatMathImmediateInstruction {

    public FloatSubImmediateInstruction(short value) {
        super(value);
    }

    @Override
    float applyOperation(float acc, float value) {
        return acc - value;
    }
    
}