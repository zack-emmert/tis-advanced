package cc.emmert.tisadvanced.instruction.arithmetic;

public class FloatAddImmediateInstruction extends AbstractFloatMathImmediateInstruction {

    public FloatAddImmediateInstruction(final short value) {
        super(value);
    }

    @Override
    float applyOperation(float acc, float value) {
        return acc + value;
    }

}