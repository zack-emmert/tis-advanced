package cc.emmert.tisadvanced.instruction.arithmetic;

import cc.emmert.tisadvanced.util.HalfFloat;
import cc.emmert.tisadvanced.util.NumUtils;
import li.cil.tis3d.common.module.execution.Machine;
import li.cil.tis3d.common.module.execution.MachineState;
import li.cil.tis3d.common.module.execution.instruction.Instruction;
import li.cil.tis3d.common.module.execution.target.Target;
import li.cil.tis3d.common.module.execution.target.TargetInterface;

public abstract class AbstractFloatMathInstruction implements Instruction {
    private final Target source;

    public AbstractFloatMathInstruction(final Target source) {
        this.source = source;
    }

    public final void step(final Machine machine) {
        final TargetInterface sourceInterface = machine.getInterface(source);

        if (!sourceInterface.isReading()) {
            sourceInterface.beginRead();
        }
        if (sourceInterface.canTransfer()) {
            doStep(machine, sourceInterface.read());
        }
    }

    private void doStep(final Machine machine, final short value) {
        final MachineState state = machine.getState();

        float floatAcc = applyOperation(HalfFloat.toFloat(state.acc), HalfFloat.toFloat(value));
        floatAcc = NumUtils.clampFloat(floatAcc);
        state.acc = HalfFloat.toHalf(floatAcc);
        state.pc++;
    }

    abstract float applyOperation(float acc, float value);
}
