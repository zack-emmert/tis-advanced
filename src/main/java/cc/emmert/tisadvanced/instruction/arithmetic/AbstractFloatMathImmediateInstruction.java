package cc.emmert.tisadvanced.instruction.arithmetic;

import cc.emmert.tisadvanced.util.HalfFloat;
import cc.emmert.tisadvanced.util.NumUtils;
import li.cil.tis3d.common.module.execution.Machine;
import li.cil.tis3d.common.module.execution.MachineState;
import li.cil.tis3d.common.module.execution.instruction.Instruction;

public abstract class AbstractFloatMathImmediateInstruction implements Instruction {
    private final float value;

    public AbstractFloatMathImmediateInstruction(final short value) {
        this.value = HalfFloat.toFloat(value);
    }

    public final void step(final Machine machine) {
        final MachineState state = machine.getState();

        float floatAcc = this.applyOperation(HalfFloat.toFloat(state.acc), this.value);
        floatAcc = NumUtils.clampFloat(floatAcc);
        state.acc = HalfFloat.toHalf(floatAcc);
        state.pc++;
    }

    abstract float applyOperation(float acc, float value);
}
