package cc.emmert.tisadvanced.instruction.arithmetic;

import li.cil.tis3d.common.module.execution.target.Target;

public class FloatMulInstruction extends AbstractFloatMathInstruction {

    public static final String NAME = "MULF";

    public FloatMulInstruction(Target source) {
        super(source);
    }

    @Override
    float applyOperation(float acc, float value) {
        return acc * value;
    }
    
}