package cc.emmert.tisadvanced.instruction.arithmetic;

import li.cil.tis3d.common.module.execution.target.Target;

public class FloatDivInstruction extends AbstractFloatMathInstruction {

    public static final String NAME = "DIVF";

    public FloatDivInstruction(Target source) {
        super(source);
    }

    @Override
    float applyOperation(float acc, float value) {
        return acc / value;
    }
    
}