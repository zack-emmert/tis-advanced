package cc.emmert.tisadvanced.instruction.conditional;

import cc.emmert.tisadvanced.util.HalfFloat;
import li.cil.tis3d.common.module.execution.MachineState;

public class FloatJumpLessThanZeroInstruction extends AbstractFloatJumpInstruction {
    public static final String NAME = "JLZF";

    public FloatJumpLessThanZeroInstruction(final String label) {
        super(label);
    }

    @Override
    boolean testCondition(final MachineState state) {
        return HalfFloat.less(state.acc, HalfFloat.NEGATIVE_ZERO);
    }
}