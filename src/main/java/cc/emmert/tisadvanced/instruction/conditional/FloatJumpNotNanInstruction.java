package cc.emmert.tisadvanced.instruction.conditional;

import cc.emmert.tisadvanced.util.HalfFloat;
import li.cil.tis3d.common.module.execution.MachineState;

public class FloatJumpNotNanInstruction extends AbstractFloatJumpInstruction {
    public static final String NAME = "JNN";

    public FloatJumpNotNanInstruction(final String label) {
        super(label);
    }

    @Override
    boolean testCondition(final MachineState state) {
        return !HalfFloat.isNaN(state.acc);
    }
}
