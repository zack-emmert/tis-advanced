package cc.emmert.tisadvanced.instruction.conditional;

import cc.emmert.tisadvanced.util.HalfFloat;
import li.cil.tis3d.common.module.execution.MachineState;

public class FloatJumpNotZeroInstruction extends AbstractFloatJumpInstruction {
    public static final String NAME = "JNZF";

    public FloatJumpNotZeroInstruction(final String label) {
        super(label);
    }

    @Override
    boolean testCondition(final MachineState state) {
        return !HalfFloat.equals(state.acc, HalfFloat.POSITIVE_ZERO);
    }
}