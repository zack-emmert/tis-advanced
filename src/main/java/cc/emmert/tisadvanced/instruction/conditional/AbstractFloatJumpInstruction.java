package cc.emmert.tisadvanced.instruction.conditional;

import li.cil.tis3d.common.module.execution.Machine;
import li.cil.tis3d.common.module.execution.MachineState;
import li.cil.tis3d.common.module.execution.instruction.Instruction;

public abstract class AbstractFloatJumpInstruction implements Instruction {

    final String label;

    public AbstractFloatJumpInstruction(final String label) {
        this.label = label;
    }

    @Override
    public final void step(final Machine machine) {
        final MachineState state = machine.getState();
        if (testCondition(state)) {
            state.pc = state.labels.get(label);
        } else {
            state.pc++;
        }
    }

    abstract boolean testCondition(MachineState state);
    
}
