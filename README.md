# Welcome to TIS Advanced

TIS Advanced adds floating point operation support to the TIS-3D Execution Module, and a new ASIC Module do do advanced floating point calculations. See the wiki for more information.
